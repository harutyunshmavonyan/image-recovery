import cv2
import numpy as np
import argparse
import re

f = open("spoiledCoords.json", "r")
coords = f.read()
f.close()
coords = re.split('; |, |\*|\n| ', coords)

spoiledCoordsList = np.array([[int(coords[0]), int(coords[1])], 
                                  [int(coords[2]), int(coords[3])]])

def recoverChannel(restoredFileName):
    global spoiledCoordsList

    f = open(restoredFileName, "r") 
    recoveredData = f.read()
    f.close()

    recoveredDataList = np.array(recoveredData.split("\n"), dtype=np.uint8)
    destroyedShape = (spoiledCoordsList[1][1] - spoiledCoordsList[0][1], spoiledCoordsList[1][0] - spoiledCoordsList[0][0])
    recoveredDataList = np.reshape(recoveredDataList, destroyedShape) 
    return np.array([recoveredDataList[len(recoveredDataList) - 1 - i] for i in range(0, len(recoveredDataList))], dtype=np.uint8)


spoiledImage = cv2.imread("spoiled.jpg")
cv2.imshow("spoiledImage", spoiledImage)

restoredPart = cv2.merge((recoverChannel("restoredBlue.txt"), recoverChannel("restoredGreen.txt"), recoverChannel("restoredRed.txt")))
cv2.imshow("restoredPart", spoiledImage)
cv2.imwrite("restoredPart.jpg", restoredPart)

spoiledImage[spoiledCoordsList[0][1]:spoiledCoordsList[1][1], spoiledCoordsList[0][0]:spoiledCoordsList[1][0]] = restoredPart

cv2.imshow("recovered", spoiledImage)
cv2.waitKey(0)

cv2.imwrite("recovered.jpg", spoiledImage)