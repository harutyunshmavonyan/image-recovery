import cv2
import numpy as np
import argparse

coordinates = []

refPt = []
 
def addCoordinate(event, x, y, flags, param):
	global coordinates
 
	if event == cv2.EVENT_LBUTTONDOWN:
		refPt.append((x, y))

def writeToFile(image, fileName):
	flatten = ""

	for i in range(image.shape[1]):
		for j in range(image.shape[0]):
			flatten += str(i) + " " + str(image.shape[0] - j) + " " + str(image[j][i]) + "\n" 

	flatten = flatten[:-1]

	f = open(fileName, 'w+')
	f.write(flatten)
	f.close()
        
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())
 
image = cv2.imread(args["image"])



cv2.namedWindow("image")
cv2.setMouseCallback("image", addCoordinate)
 
while len(refPt) < 2:
	cv2.imshow("image", image)
	key = cv2.waitKey(1) & 0xFF
 
spoiled = np.copy(image)
zeros = np.zeros((refPt[1][1]-refPt[0][1], refPt[1][0]-refPt[0][0], 3), dtype = np.uint8)
spoiled[refPt[0][1]:refPt[1][1], refPt[0][0]:refPt[1][0]] = zeros

cv2.imwrite("spoiled.jpg", spoiled)

cv2.imshow("Spoiled", spoiled)
cv2.waitKey(0)
 
cv2.destroyAllWindows()

gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
gray_spoiled = cv2.cvtColor(spoiled, cv2.COLOR_BGR2GRAY)

blue = image[:,:,0]
blue_spoiled = spoiled[:,:,0]

green = image[:,:,1]
green_spoiled = spoiled[:,:,1]

red = image[:,:,2]
red_spoiled = spoiled[:,:,2]

writeToFile(gray, "image.json")
writeToFile(gray_spoiled, "spoiled.json")

writeToFile(blue, "blue.json")
writeToFile(blue_spoiled, "spoiledBlue.json")

writeToFile(green, "green.json")
writeToFile(green_spoiled, "spoiledGreen.json")

writeToFile(red, "red.json")
writeToFile(red_spoiled, "spoiledred.json")


f = open("size.json", 'w+')
f.write(str(gray.shape[1]) + " " + str(gray.shape[0]))
f.close()

f = open("spoiledCoords.json", 'w+')
f.write(str(refPt[0][0]) + " " + str(refPt[0][1]) + "\n" + str(refPt[1][0]) + " " + str(refPt[1][1]))
f.close()